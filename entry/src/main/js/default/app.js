import bridge from './common/js/bridge.js'
import bus from './common/js/event.js'

export default {
    bus,
    data: {
        screenWidth: 0,
        screenHeight: 0,
        statusBarHeight: 0,
        ppi: 0
    },
    onCreate() {},
    onDestroy() {},
    async getScreenWidth() {
        if (this.screenWidth) return this.screenWidth

        const data = await bridge.getScreenWidthAndHeight()
        this.screenWidth = data.width
        return this.screenWidth
    },
    async getScreenHeight() {
        if (this.screenHeight) return this.screenHeight

        const data = await bridge.getScreenWidthAndHeight()
        this.screenHeight = data.height
        return this.screenHeight
    },
    async getStatusBarHeight() {
        if (this.statusBarHeight) return this.statusBarHeight

        this.statusBarHeight = await bridge.getStatusBarHeight()
        return this.statusBarHeight
    },
    async getPPI() {
        if (this.ppi) return this.ppi

        this.ppi = await bridge.getPPI()
        return this.ppi
    }
};
