export default {
    data: {
        images: [],
        x: 0,
        y: 0,
        sizes: {}
    },
    onInit() {
        this.buildImages()
    },
    buildImages() {
        for (let i = 0; i < 5; i++) {
            this.images.push('/common/images/ohos.jpg')
            this.images.push('/common/images/bg_ios.jpg')
            this.images.push('/common/images/print.jpg')
            this.images.push('/common/images/bg_tv.jpg')
        }
    },
    touchStart(e) {
        this.x = e.touches[0].globalX
        this.y = e.touches[0].globalY
    },
    itemClick(e) {
        const index = e.detail
        const size = this.sizes['img' + index]

        if (!size) return

        this.$child('preview').show(this.images[index], this.x, this.y, size)
    },
    onImageLoaded(e) {
        const data = e.detail
        this.sizes['img' + data.index] = data.size
    }
}
