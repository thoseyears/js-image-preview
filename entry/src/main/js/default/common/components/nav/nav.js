export default {
    data: {
        statusBarHeight: 0,
        titleString: ''
    },
    props: {
        background: {
            type: String,
            default: '#FFFFFF'
        },
        navHeight: {
            type: Number,
            default: 52
        },
        title: String,
        titleColor: {
            type: String,
            default: '#333333'
        }
    },
    async onInit() {
        this.initTitle();
        this.statusBarHeight = await this.$app.getStatusBarHeight()
    },
    initTitle() {
        this.titleString = this.title || this.$t('strings.title')
    }
}
