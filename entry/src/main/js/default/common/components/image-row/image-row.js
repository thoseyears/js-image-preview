export default {
    data: {
        data: [],
        height: 1
    },
    props: {
        count: {
            type: Number,
            default: 3
        },
        index: Number,
        items: {
            type: Array,
            default: []
        }
    },
    onInit() {
        this.data = this.items
        const c = parseInt(this.count)
        if (this.data.length < c) {
            const diff = c - this.data.length;
            for (let i = 0; i < diff; i++) {
                this.data.push('')
            }
        }
    },
    onPageShow() {
        this.initHeight()
    },
    async initHeight() {
        const el = this.$element()
        let width
        if (el.getBoundingClientRect) {
            width = el.getBoundingClientRect().width
        } else {
            width = await this.$app.getScreenWidth()
        }

        this.height = width / parseInt(this.count)
    },
    itemClick(i) {
        setTimeout(() => {
            this.$emit('click', this.getRealIndex(i))
        }, 50)
    },
    getRealIndex(i) {
        return parseInt(this.index) * parseInt(this.count) + i
    }
}
