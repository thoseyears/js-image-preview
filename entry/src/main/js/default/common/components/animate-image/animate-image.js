export default {
    data: {
        opacity: 0,
        finished: false,
        options: {
            duration: 300,
            easing: 'ease-in'
        },
        frames: [
            { transform: { translate: '360px 100px', scale: '0.3' }, opacity: 0 },
            { transform: { translate: '0px 0px', scale: '1' }, opacity: 1 }
        ]
    },
    props: {
        src: String,
        height: Number,
        index: Number
    },
    onInit() {
        setTimeout(() => {
            this.options.delay = parseInt(this.index) * 30
            const animator = this.createAnimation()
            animator.play()
            animator.onfinish = this.finish
        }, 50)
    },
    createAnimation() {
        return this.$element('img').animate(this.frames, this.options)
    },
    finish() {
        this.finished = true
        this.opacity = 1
    },
    complete(data) {
        const size = {
            width: data.width,
            height: data.height
        }

        const bus = this.$app.$def.bus
        bus.$emit('imageLoaded', {
            index: this.index, size
        })
    }
}
