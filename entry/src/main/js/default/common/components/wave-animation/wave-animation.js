export default {
    data: {
        animator: null,
        hover: false,
        earlyEnded: false,
        options: {
            duration: 150,
            easing: 'ease-in',
            fill: 'forwards'
        }
    },
    props: {
        color: {
            type: String,
            default: '#99FFFFFF'
        }
    },
    touchStart() {
        if (this.animator) {
            this.animator.finish()
        }

        this.animator = this.createAnimation(false)
        this.animator.play()
        this.animator.onfinish = () => {
            this.hover = true
            this.animator = null
            if (this.earlyEnded) {
                setTimeout(() => {
                    this.touchEnd()
                }, 10)
            }
        }
    },
    touchEnd() {
        if (this.animator) {
            this.earlyEnded = true
            return
        }

        this.earlyEnded = false
        this.animator = this.createAnimation(true)
        this.animator.play()
        this.animator.onfinish = () => {
            this.hover = false
            this.animator = null
        }
    },
    createFrames(reverse) {
        if (reverse) {
            return [
                { transform: { scale: '1.5' }, backgroundColor: this.color },
                { transform: { scale: '1.5' }, backgroundColor: '#00000000' }
            ]
        } else {
            return [
                { transform: { scale: '0' }, backgroundColor: this.color },
                { transform: { scale: '1.5' }, backgroundColor: this.color }
            ]
        }
    },
    createAnimation(reverse) {
        return this.$element('animation').animate(this.createFrames(reverse), this.options)
    }
}
