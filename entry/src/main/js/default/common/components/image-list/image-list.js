export default {
    data: {
        images: []
    },
    props: {
        items: {
            type: Array,
            default: []
        },
        column: {
            type: Number,
            default: 3
        }
    },
    onInit() {
        this.handleData()
        const bus = this.$app.$def.bus
        bus.$on('imageLoaded', data => {
            this.$emit('imageLoaded', data)
        })
    },
    handleData() {
        const count = parseInt(this.column)
        let arr = []
        this.items.forEach((v, i) => {
            arr.push(v)
            if ((i !== 0 && i % count === count - 1) || i === this.items.length - 1) {
                this.images.push(arr)
                arr = []
            }
        })
    },
    rowClicked(e) {
        this.$emit('itemClick', e.detail)
    }
}
