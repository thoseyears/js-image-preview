import color from './color.js'
import AnimationHelper from './cls/AnimationHelper.js'

/**
 * 下滑手势帮助类
 */
export default {
    helper: new AnimationHelper(),
    firstTime: true,
    process: 0,
    vy: 0,
    movingDown: false,

    /**
     * 处理滚动事件，如果时下滑手势则拦截
     * @param that 上下文
     * @param distanceX x轴滚动距离
     * @param distanceY y轴滚动距离
     * @param diffX x轴上一次触摸到这一次之间的距离差
     * @param diffY y轴上一次触摸到这一次之间的距离差
     * @param up 手指是否抬起
     * @return 是否拦截事件
     */
    handleScroll(that, distanceX, distanceY, diffX, diffY, up) {
        if (that.image.currScale != 1) return false

        if (this.firstTime) {
            this.movingDown = this.isMovingDown(diffX, diffY)
            this.firstTime = false
        }

        if (!this.movingDown) return false

        const process = distanceY / that.height * 1.5
        this.process = process

        if (up) return true

        const a = 1 - process
        that.image.bg = color.adjustAlpha(a, that.background)

        this.invalidate(that, that.image.initImageWidth * a, that.image.initImageHeight * a,
            distanceX, distanceY)

        return true
    },

    /**
     * 处理fling事件，如果是由于下滑手势产生的fling则拦截
     * @param that 上下文
     * @param vx x轴速度
     * @param vy y轴速度
     * @return 是否拦截事件
     */
    handleFling(that, vx, vy) {
        if (!this.movingDown) {
            this.reset()
            return false
        }
        this.vy = vy
        this.onUp(that)
        return true
    },

    onUp(that) {
        if (this.process > 0.5 || Math.abs(this.vy) >= 500) {
            that.hide()
            this.reset()
        } else {
            this.restore(that)
        }
    },

    restore(that) {
        this.helper.start()

        const startW = that.image.imageWidth
        const startH = that.image.imageHeight

        const finalW = that.image.initImageWidth
        const finalH = that.image.initImageHeight

        const startTX = that.trans.transX
        const startTY = that.trans.transY

        this.helper.run(() => {
            return this.helper.compute(p => {
                const w = startW + parseInt(p * (finalW - startW))
                const h = startH + parseInt(p * (finalH - startH))
                const x = startTX + parseInt(p * -startTX)
                const y = startTY + parseInt(p * -startTY)
                this.invalidate(that, w, h, x, y)

                const a = 1 - this.process
                that.image.bg = color.adjustAlpha(a + p * (1 - a), that.background)
            }, () => {
                this.invalidate(that, finalW, finalH, 0, 0)
                that.image.bg = color.adjustAlpha(1, that.background)
                this.reset()
            })
        }, () => {})
    },

    invalidate(that, w, h, x, y) {
        that.image.imageWidth = w
        that.image.imageHeight = h

        that.trans.transX = x
        that.trans.transY = y

        that.clearCanvas = true
        that.loadImage()
    },

    reset() {
        this.firstTime = true
        this.movingDown = false
        this.process = 0
        this.vy = 0
    },

    isMovingDown(diffX, diffY) {
        return diffY > 0 && Math.abs(diffX) <= 3
    }
}