import AnimationHelper from './AnimationHelper.js'

/**
 * 缩放动画
 */
class ScaleAnimator {
    constructor(interval, w, h) {
        this.width = w
        this.height = h

        this.currScale = 1
        this.targetScale = 1
        this.startTime = 0

        this.startWidth = 0
        this.startHeight = 0
        this.currWidth = 0
        this.currHeight = 0
        this.finalWidth = 0
        this.finalHeight = 0

        this.startX = 0
        this.startY = 0
        this.currX = 0
        this.currY = 0
        this.finalX = 0
        this.finalY = 0

        this.helper = new AnimationHelper(interval)
    }

    /**
     * 设置一倍缩放下，容器大小
     */
    setSize(w, h) {
        this.width = w
        this.height = h
    }

    /**
     * 获取一倍缩放下，容器的宽
     */
    getWidth() {
        return this.width
    }

    /**
     * 获取一倍缩放下，容器的高
     */
    getHeight() {
        return this.height
    }

    /**
     * 开启缩放动画
     * @param scale 缩放背书
     * @param startX 动画开始时的x轴偏移
     * @param startY 动画开始时的y轴偏移
     * @param finalX 最终的x轴偏移
     * @param finalY 最终的y轴偏移
     * @param duration 动画持续时间
     */
    scale(scale, startX, startY, finalX, finalY, duration) {
        if (scale === this.currScale) return

        this.helper.start(duration)

        this.targetScale = scale

        this.startWidth = this.currWidth = this.width * this.currScale
        this.startHeight = this.currHeight = this.height * this.currScale

        this.finalWidth = this.width * this.targetScale
        this.finalHeight = this.height * this.targetScale

        this.startX = this.currX = startX
        this.startY = this.currY = startY

        this.finalX = finalX
        this.finalY = finalY
    }

    /**
     * 运行缩放动画
     * @param scale 缩放倍数
     * @param startX 动画开始时的x轴偏移
     * @param startY 动画开始时的y轴偏移
     * @param finalX 最终的x轴偏移
     * @param finalY 最终的y轴偏移
     * @param task 动画每一帧的回调
     * @param duration 动画持续时间
     */
    runScale(scale, startX, startY, finalX, finalY, task, duration) {
        this.scale(scale, startX, startY, finalX, finalY, duration)
        this.helper.run(() => {
            return this.compute()
        }, () => {
            if (task) task(this.currWidth, this.currHeight, this.currX, this.currY)
        })
    }

    /**
     * 计算下一帧动画的容器的宽高与偏移量，并判断动画是否结束
     */
    compute() {
        return this.helper.compute(progress => {
            this.currWidth = this.startWidth + parseInt(progress * (this.finalWidth - this.startWidth))
            this.currHeight = this.startHeight + parseInt(progress * (this.finalHeight - this.startHeight))

            this.currX = this.startX + parseInt(progress * (this.finalX - this.startX))
            this.currY = this.startY + parseInt(progress * (this.finalY - this.startY))

            if (this.currWidth == this.finalWidth && this.currHeight == this.finalHeight) {
                this.finish()
            }
        }, () => {
            this.currWidth = this.finalWidth
            this.currHeight = this.finalHeight

            this.currX = this.finalX
            this.currY = this.finalY

            this.currScale = this.targetScale
        })
    }

    /**
     * 结束动画
     */
    finish() {
        this.helper.finish()
        this.currScale = this.targetScale
    }

    /**
     * 重置运行时
     */
    reset() {
        this.helper.finish()
        this.currScale = 1
    }
}

export default ScaleAnimator