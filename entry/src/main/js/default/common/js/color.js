const ALPHA_INT = 255

export default {
    /**
     * 获取alpha的int值，0-255
     * @param alpha 0-1的alpha值
     * @return int值
     */
    alphaValue(alpha) {
        return Math.round(alpha * ALPHA_INT)
    },

    /**
     * 改变颜色的alpha值
     * @param alpha 0-1的alpha值
     * @param color 字符串类型的颜色，如#FFFFFF
     * @return 改变alpha后的字符串类型的颜色
     */
    adjustAlpha(alpha, color) {
        const v = this.alphaValue(alpha)
        const hex = v.toString(16)

        const l = color.length
        const body = color.substring(l - 6, l)
        return '#' + hex + body
    }
}