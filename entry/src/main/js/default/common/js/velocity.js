import java from './call-java.js'

const NAME = 'com.ryan.ohos.imagepreview.services.velocity.VelocityService'

const CODE_ADD_EVENT = 0
const CODE_GET_VELOCITY = 2
const CODE_CLEAR = 3

const DOWN = 1
const MOVE = 3
const UP = 2

/**
 * 添加触摸事件
 * @param ac 触摸事件类型
 * @param x x坐标
 * @param y y坐标
 */
const addEvent = (ac, x, y) => {
    const action = java.createAction(NAME, CODE_ADD_EVENT, {
        action: ac, x, y
    })
    java.call(action).then(() => {})
}

/**
 * 根据已添加的触摸事件序列，获取对于的速度
 * @return x与y轴的速度 { x: 1000, y: 1000 }
 */
const getVelocity = async () => {
    const action = java.createAction(NAME, CODE_GET_VELOCITY)
    return await java.call(action)
}

/**
 * 清除已添加的事件
 */
const clear = () => {
    const action = java.createAction(NAME, CODE_CLEAR)
    java.call(action).then(() => {})
}

export default {
    DOWN, MOVE, UP, addEvent, getVelocity, clear
}