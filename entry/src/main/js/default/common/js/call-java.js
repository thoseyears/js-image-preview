const ABILITY_TYPE_EXTERNAL = 0
const ABILITY_TYPE_INTERNAL = 1

const ACTION_SYNC = 0
const ACTION_ASYNC = 1

/**
 * 生成action，用于callAbility
 * @param name 待调用的Ability的完整类名
 * @param code 用于区分行为的code
 * @param data 待传递的数据
 * @return action
 */
const createAction = (name, code, data) => {
    const action = {}
    action.bundleName = 'com.ryan.ohos.imagepreview'
    action.abilityName = name
    action.abilityType = ABILITY_TYPE_EXTERNAL
    action.syncOption = ACTION_SYNC
    action.messageCode = code
    if (data) action.data = data

    return action
}

/**
 * 调用Ability
 * @param action action
 * @return Ability返回的数据
 */
const call = async action => {
    let result = await FeatureAbility.callAbility(action)
    result = JSON.parse(result)
    return result.data
}

export default {
    createAction, call
}