/**
 * 图片相关计算帮助类
 */
export default {
    /**
     * 根据屏幕宽高，与图片原始的宽高，决定最终显示的宽高
     * @param origin 图片原始的宽高 { width: 1, height: 1 }
     * @param screenWidth 屏幕宽
     * @param screenHeight 屏幕宽高
     * @return 最终显示的宽高 { width: 1, height: 1 }
     */
    getInitImageSize(origin, screenWidth, screenHeight) {
        let percent
        if (origin.width / origin.height > screenWidth / screenHeight) {
            percent = screenWidth / origin.width
        } else {
            percent = screenHeight / origin.height
        }

        return {
            width: parseInt(origin.width * percent),
            height: parseInt(origin.height * percent)
        }
    },

    /**
     * 计算绘制图片时的最大相对偏移量
     * @param size 图片的大小
     * @param screenSize 屏幕的大小
     * @return 最大偏移量
     */
    calulateMaxTranslate(size, screenSize) {
        const maxTrans = (size - screenSize) / 2
        return maxTrans < 0 ? 0 : maxTrans
    },

    /**
     * 限制相对偏移量不超过最大偏移量
     * @param trans 偏移量
     * @param size 图片的大小
     * @param screenSize 屏幕的大小
     * @return 最终的偏移量
     */
    restrictTranslate(trans, size, screenSize) {
        const maxTrans = this.calulateMaxTranslate(size, screenSize)
        if (Math.abs(trans) > maxTrans) {
            return trans < 0 ? -maxTrans : maxTrans
        }
        return trans
    },

    /**
     * 计算绘制图片时的真实偏移量，以屏幕左上角为原点
     * @param size 图片大小
     * @param screenSize 屏幕大小
     * @param trans 偏移量，以屏幕中心为原点
     * @return 真实偏移量
     */
    calculateDrawTranslate(size, screenSize, trans) {
        return parseInt((screenSize - size) / 2 + trans)
    },

    /**
     * 计算绘制图片时的相对偏移量，以屏幕中心点为原点
     * @param size 图片大小
     * @param screenSize 屏幕大小
     * @param trans 偏移量，以屏幕左上角为原点
     * @return 相对偏移量
     */
    convertDrawTranslate(size, screenSize, trans) {
        return parseInt(trans - (screenSize - size) / 2)
    },

    /**
     * 计算图片缩放时的相对偏移量，以屏幕中心为原点
     * @param p 缩放时的中心点坐标
     * @param scale 缩放比例
     * @param size 图片大小
     * @param screenSize 屏幕大小
     * @param lastTrans 上一次的相对偏移量
     * @return 偏移量
     */
    calculateScaleTranslate(p, scale, size, screenSize, lastTrans) {
        const finalSize = size * scale
        lastTrans = this.calculateDrawTranslate(size * (scale - 1), screenSize, lastTrans)
        const origin = lastTrans - p
        return this.restrictTranslate(this.convertDrawTranslate(finalSize, screenSize, origin), finalSize, screenSize)
    }
}