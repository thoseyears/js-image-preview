import java from './call-java.js'

const NAME = 'com.ryan.ohos.imagepreview.services.UtilService'

const CODE_STATUS_BAR_HEIGHT = 0
const CODE_SCREEN_WIDTH_HEIGHT = 1
const CODE_PPI = 2

/**
 * 获取状态栏高度
 * @return 状态栏高度
 */
const getStatusBarHeight = async () => {
    const action = java.createAction(NAME, CODE_STATUS_BAR_HEIGHT)
    const result = await java.call(action)
    return parseInt(result)
}

/**
 * 获取屏幕的宽高
 * { width: 1, height: 1 }
 * @return 屏幕的宽高
 */
const getScreenWidthAndHeight = async () => {
    const action = java.createAction(NAME, CODE_SCREEN_WIDTH_HEIGHT)
    return await java.call(action)
}

/**
 * 获取屏幕的PPI
 * @return PPI
 */
const getPPI = async () => {
    const action = java.createAction(NAME, CODE_PPI)
    const result = await java.call(action)
    return parseFloat(result)
}

export default {
    getStatusBarHeight, getScreenWidthAndHeight, getPPI
}