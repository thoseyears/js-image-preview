package com.ryan.ohos.imagepreview.services.velocity;

import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public class TouchEventProxy extends TouchEvent {

    private int action;
    private long time;
    private MmiPoint point;

    public void setAction(int action) {
        this.action = action;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setPoint(float x, float y) {
        this.point = new MmiPoint(x, y);
    }

    @Override
    public int getAction() {
        return action;
    }

    @Override
    public int getIndex() {
        return 0;
    }

    @Override
    public long getStartTime() {
        return time;
    }

    @Override
    public int getPhase() {
        return 0;
    }

    @Override
    public MmiPoint getPointerPosition(int i) {
        return point;
    }

    @Override
    public void setScreenOffset(float v, float v1) {}

    @Override
    public MmiPoint getPointerScreenPosition(int i) {
        return point;
    }

    @Override
    public int getPointerCount() {
        return 1;
    }

    @Override
    public int getPointerId(int i) {
        return 0;
    }

    @Override
    public float getForce(int i) {
        return 0;
    }

    @Override
    public float getRadius(int i) {
        return 0;
    }

    @Override
    public int getSourceDevice() {
        return 0;
    }

    @Override
    public String getDeviceId() {
        return "";
    }

    @Override
    public int getInputDeviceId() {
        return 0;
    }

    @Override
    public long getOccurredTime() {
        return time;
    }
}
