package com.ryan.ohos.imagepreview.utils;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

public class WindowUtil {

    public static final int NAVIGATION_HEIGHT = 39;
    public static final int MAX_STATUS_BAR_HEIGHT = 50;
    public static final int DEFAULT_STATUS_BAR_HEIGHT = 25;

    private volatile static float ASPECT_RATIO = 1.97f;

    /**
     * 获取底部导航栏高度px
     *
     * @param context context
     * @return 高度px
     */
    public static int getNavigationHeightInPixels(Context context) {
        return AttrHelper.vp2px(NAVIGATION_HEIGHT, context);
    }

    /**
     * 获取状态栏高度vp
     *
     * @param context context
     * @return 高度vp
     */
    public static int getStatusBarHeight(Context context) {
        int result;
        Point point1 = new Point();
        Point point2 = new Point();

        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        display.getSize(point1);
        display.getRealSize(point2);

        result = (int) (point2.getPointY() - point1.getPointY());

        float densityPixels = AttrHelper.getDensity(context);
        result = (int) (result / densityPixels);

        if (result > MAX_STATUS_BAR_HEIGHT) {
            result -= NAVIGATION_HEIGHT;
        }

        if (result <= 0) {
            result = DEFAULT_STATUS_BAR_HEIGHT;
        }

        return result;
    }

    /**
     * 获取状态栏高度px
     *
     * @param context context
     * @return 高度px
     */
    public static int getStatusBarHeightInPixels(Context context) {
        return AttrHelper.vp2px(getStatusBarHeight(context), context);
    }

    /**
     * 隐藏状态栏
     *
     * @param window window
     * @param isHide 是否隐藏
     */
    public static void hideStatusBar(Window window, boolean isHide) {
        if (isHide) {
            window.setStatusBarColor(Color.TRANSPARENT.getValue());
            window.addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        } else {
            window.clearFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        }
    }

    /**
     * 是否隐藏底部导航栏
     *
     * @param root 根布局
     * @return 是否隐藏底部导航栏
     */
    public static boolean isHideNavigationBar(ComponentContainer root) {
        if (root == null) {
            return false;
        }
        return (root.getVisibility() & WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION)
                == WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION;
    }


    /**
     * 判断当前手机是否为全面屏--通过纵横比
     *
     * @param context context
     * @return 纵横比
     */
    public static boolean isFullScreenDevice(Context context) {
        if (context == null) {
            return false;
        }
        return getAspectRatio(context) >= ASPECT_RATIO;
    }

    /**
     * 获取手机纵横比
     *
     * @param context context
     * @return 纵横比
     */
    public static float getAspectRatio(Context context) {
        if (context == null) {
            return 0f;
        }
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        Point point = new Point();
        display.getRealSize(point);
        float width, height;
        if (point.getPointX() < point.getPointY()) {
            width = point.getPointX();
            height = point.getPointY();
        } else {
            width = point.getPointY();
            height = point.getPointX();
        }
        return height / width;
    }

    /**
     * 获取屏幕宽
     * @param context context
     * @return int
     */
    public static int getWindowWidth(Context context) {
        return context.getResourceManager().getDeviceCapability().width;
    }

    /**
     * 获取屏幕高
     * @param context context
     * @return int
     */
    public static int getWindowHeight(Context context) {
        return context.getResourceManager().getDeviceCapability().height + getStatusBarHeight(context);
    }

    public static float getPPI(Context context) {
        return DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().densityPixels * 160.0f;
    }
}
