package com.ryan.ohos.imagepreview;

import com.ryan.ohos.imagepreview.utils.WindowUtil;
import ohos.ace.ability.AceAbility;
import ohos.aafwk.content.Intent;

public class MainAbility extends AceAbility {
    @Override
    public void onStart(Intent intent) {
        WindowUtil.hideStatusBar(getWindow(), true);

        super.onStart(intent);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
