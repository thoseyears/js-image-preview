package com.ryan.ohos.imagepreview.services;

import com.ryan.ohos.imagepreview.utils.WindowUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.rpc.*;

public class UtilService extends Ability {

    private static final int CODE_STATUS_BAR_HEIGHT = 0;
    private static final int CODE_SCREEN_WIDTH_HEIGHT = 1;
    private static final int CODE_PPI = 2;

    private final ServiceRemote remote = new ServiceRemote();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    protected IRemoteObject onConnect(Intent intent) {
        super.onConnect(intent);
        return remote.asObject();
    }

    @Override
    public void onDisconnect(Intent intent) {}

    class ServiceRemote extends RemoteObject implements IRemoteBroker {
        ServiceRemote() {
            super("UtilService");
        }

        @Override
        public boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) {
            switch (code) {
                case CODE_STATUS_BAR_HEIGHT:
                    String statusBarHeight = WindowUtil.getStatusBarHeight(getContext()) + "";
                    reply.writeString("{\"code\":0,\"data\":" + statusBarHeight + "}");
                    break;
                case CODE_SCREEN_WIDTH_HEIGHT:
                    String width = WindowUtil.getWindowWidth(getContext()) + "";
                    String height = WindowUtil.getWindowHeight(getContext()) + "";
                    reply.writeString("{\"code\":0,\"data\":{\"width\":" + width + ", \"height\":" + height + "}}");
                    break;

                case CODE_PPI:
                    String ppi = WindowUtil.getPPI(getContext()) + "";
                    reply.writeString("{\"code\":0,\"data\":" + ppi + "}");
                    break;

            }
            return true;
        }

        @Override
        public IRemoteObject asObject() {
            return this;
        }
    }
}